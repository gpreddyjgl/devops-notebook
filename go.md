## Go

### Cheat Sheet

* Run your program:

```
go run <file path>
```

* Compile your app package:

```
go build <package path>
```

* Install and run from a remote location:

```
go install github.com/bregman-arie/myApp
bin/myApp
```
