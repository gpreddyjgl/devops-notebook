# devops-notebook


## General
Name | Description
:------|:------:
[DevOps Roadmap](https://github.com/kamranahmedse/developer-roadmap) | DevOps skills roadmap. What to to learn at each step.
[Periodic table of DevOps tools](https://xebialabs.com/periodic-table-of-devops-tools) | Table of DevOps tools per category
[DevOps tool chest](https://xebialabs.com/the-ultimate-devops-tool-chest/#tool-chest-type) | List of DevOps tools


## Practice and learning sites
Name | Description
:------|:------:
[KataKoda](https://www.katacoda.com) | Linux, Docker, ML, Kubernetes, ... environments and scenarios
[QWIKLBAS](https://www.qwiklabs.com) | AWS, GCP, ... lab practice
[vim-adventures](https://vim-adventures.com) | Learning VIM while playing a game


## Python
Go [here](python.md)

## Bash best practices

Go [here](bash/best_practices.md)

## Bash resources

Name | Content Type
:------ |:--------:
[Bash Wiki Hackers](http://wiki.bash-hackers.org/start) | "hold documentation of any kind about GNU Bash"
[Bash Reference Manual](https://tiswww.case.edu/php/chet/bash/bashref.html) | everything there is to know about bash
[Hacker Rank Linux Shell](https://www.hackerrank.com/domains/shell) | Bash Challenges

## Linux
Go [here](linux.md)

## Containers (Docker) 
Go [here](linux.md)

## Sites with DevOps articles

Name | Description
:------|:------:
[opensource.com](https://opensource.com) | open source related articles including DevOps


## Kubernetes

Name | Description
:------|:------:
[OperatiorHub.io](https://www.operatorhub.io) | Kubernetes native applications
[Kubernetes 101](https://medium.com/google-cloud/kubernetes-101-pods-nodes-containers-and-clusters-c1509e409e16) | Great beginner article on Kubernetes fundamental concepts


## MongoDB
Name | Description
:------|:------:
[Guru99 MongoDB Tutorial](https://www.guru99.com/what-is-mongodb.html) | MongoDB Tutorial
[Tutorialspoint Tutorial](https://www.tutorialspoint.com/mongodb) | MongoDB Tutorial


## Code challenges and practice sites


## DevOps Snippets

Name | Description
:------|:------:
[DevOpsnipp](https://www.devopsnipp.com/) | Sharing DevOps Snippets

## Utils

Name | Description
:------|:------:
[Json Formatter](https://jsonformatter.curiousconcept.com) | Json Formatter & Validator


## Machine Learning
Go [here](machine_learning.md)


## OpenStack

Name | Description
:------|:------:
[OpenStack Operator Tools](https://github.com/openstack/osops-tools-contrib) | Tools and scripts for neutron, nova, etc.
